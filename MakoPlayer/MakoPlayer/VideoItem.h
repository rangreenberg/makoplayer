//
//  VideoItem.h
//  MakoPlayer
//
//  Created by Ran Greenberg on 5/7/14.
//  Copyright (c) 2014 mako. All rights reserved.
//


@protocol VideoItem <NSObject>

@property (readonly) id data;
@property (readonly) NSString* guid;
@property (readonly) NSString* title;
@property (readonly) NSString* externalTitle;
@property (readonly) NSString* subtitle;
@property (readonly) NSString* description;
@property (readonly) NSURL* thumbnailURL;
@property (readonly) NSURL* imageURL;
@property (readonly) NSURL* videoURL;
@property (readonly) NSString* vodType;
@property (readonly) NSString* relativeLink;
@property (readonly) NSString* videoContentType;
@property (readonly) NSString* channelGuid;

@end
