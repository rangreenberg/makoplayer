//
//  BaseVideoItem.m
//  MakoTV2
//
//  Created by Sagi Mann on 9/12/13.
//  Copyright (c) 2013 Tomer Har Yoffi. All rights reserved.
//

#import "BaseVideoItem.h"




@implementation BaseVideoItem

@synthesize data = _data;



- (id)initWithData:(id)data {
    if (self = [super init]) {
        _data = data;
    }
    return self;
}



- (NSString *)guid { return [self stringForKey:@"guid"]; }
- (NSString *)title { return [self stringForKey:@"title"]; }
- (NSString *)externalTitle { return [self stringForKey:@"externalTitle"]; }
- (NSString *)subtitle { return [self stringForKey:@"subtitle"]; }
- (NSString *)description { return nil; }
- (NSURL *)thumbnailURL { return nil; }
- (NSURL *)imageURL { return nil; }
- (NSURL *)videoURL { return nil; }
- (NSString *)vodType { return nil; }
- (NSString *)relativeLink { return [self objectForKey:@"link"]; }
- (NSString *)videoContentType { return nil; }
- (NSString *)channelGuid { return nil; }


- (id)objectForKey:(NSString*)key inData:(id)data {
	if (data == nil) return nil;
	id result = [data objectForKey:key];
	if (result == [NSNull null]) result = nil;
	return result;
}



- (id)objectForKey:(NSString*)key {
    return [self objectForKey:key inData:self.data];
}



- (NSString*)stringForKey:(NSString*)key {
    return [self stringForKey:key inData:self.data];
}



- (NSString*)stringForKey:(NSString*)key inData:(id)data {
	id result = [self objectForKey:key inData:data];
	return result ? result : @"";
}



- (BOOL)boolForKey:(NSString*)key {
    return [self boolForKey:key inData:self.data];
}



- (BOOL)boolForKey:(NSString*)key inData:(id)data {
    NSNumber* result = [self objectForKey:key inData:data];
	return [result boolValue];
    
}



- (int)intForKey:(NSString*)key {
    return [self intForKey:key inData:self.data];
}



- (int)intForKey:(NSString*)key inData:(id)data {
    NSNumber* result = [self objectForKey:key inData:data];
    return result ? [result intValue] : 0;
}



- (double)doubleForKey:(NSString*)key {
    return [self doubleForKey:key inData:self.data];
}



- (double)doubleForKey:(NSString*)key inData:(id)data {
    NSNumber* result = [self objectForKey:key inData:data];
    return result ? [result doubleValue] : 0;
}



- (NSURL*)urlForKey:(NSString*)key {
    return [self urlForKey:key inData:self.data];
}



- (NSURL*)urlForKey:(NSString*)key inData:(id)data {
	NSString* result = (NSString*)[self objectForKey:key inData:data];
    if (!result) return nil;
    return [NSURL URLWithString:result];
}



@end
