//
//  ViewController.m
//  MakoPlayer
//
//  Created by Ran Greenberg on 5/1/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import "ViewController.h"
#import "MPMoviePlayerController+MakoPlayer.h"
#import "BaseVideoItem.h"
#import "SBJson4.h"


@interface ViewController ()

@property (nonatomic, strong) UIWebView *webView;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSURL *url = [NSURL URLWithString:@"http://makostore-vh.akamaihd.net/i/SHORT/TV/Programs/Eretz/S11/Nirvegali/eretz11_13_nirgali/eretz11_13_nirgali_,500,.mp4.csmil/master.m3u8"];
    
    self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:url];
    self.moviePlayer.view.frame = self.view.bounds;
    self.moviePlayer.controlStyle = MPMovieControlStyleFullscreen;
    
    // guid videoItem - example
    NSString *path = [[NSBundle mainBundle] pathForResource:@"videoItemURL" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    
    
    
    SBJson4Parser *parser = [SBJson4Parser parserWithBlock:^(id item, BOOL *stop) {
        
        NSObject *itemObject = item;
        
        if ([item isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDict = (NSDictionary*)itemObject;
            BaseVideoItem *videoItem = [[BaseVideoItem alloc] initWithData:dataDict];
            
#warning Should be taken from configuration and NOT hard coded
            NSString *path = @"http://apps.mako.co.il/mobile/_test/iphone/mako_tv/Player_Activities_test.json";
            
            [self.moviePlayer startActivitiesOnPlayerWithVideoItem:videoItem withActivitiesPath:path];
        }
    }
                                            allowMultiRoot:NO
                                           unwrapRootArray:NO
                                              errorHandler:^(NSError *error) {
                                                  klogdbg_func_arg1s(@"Faild to parse data!")
                                                  NSLog(@"%@", error);
                                              }];
    [parser parse:data];
    
    
    

    [self.moviePlayer.view setBackgroundColor:[UIColor greenColor]];
    self.moviePlayer.shouldAutoplay = YES;
    [self.view addSubview:self.moviePlayer.view];
    
    UIButton *dismissButton = [[UIButton alloc] initWithFrame:CGRectMake(400, 50, 50, 50)];
    UILabel *backLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 25, 10)];
    backLbl.font = [UIFont fontWithName:@"Arial" size:10];
    backLbl.text = @"back";
    [dismissButton addSubview:backLbl];
    dismissButton.backgroundColor = [UIColor colorWithRed:23/255.0f green:195/255.0f blue:229/255.0f alpha:1.0f];
    [dismissButton addTarget:self action:@selector(dismissPlayer) forControlEvents:UIControlEventTouchUpInside];
    
    [self.moviePlayer.view addSubview:dismissButton];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate:)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];

    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeRight animated:NO];
    [self.moviePlayer play];
}

-(void)dismissPlayer {
    
    [self.moviePlayer stopActivitiesOnPlayer];
    self.moviePlayer = nil;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(BOOL)shouldAutorotate
{
    return NO;
}

-(void)didRotate:(NSNotification*)notification {
    self.moviePlayer.view.frame = self.view.bounds;
    
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
     self.moviePlayer.view.frame = self.view.bounds;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscapeLeft;
}
@end
