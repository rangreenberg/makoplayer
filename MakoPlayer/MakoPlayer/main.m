//
//  main.m
//  MakoPlayer
//
//  Created by Ran Greenberg on 5/1/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
