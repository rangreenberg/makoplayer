//
//  ActivityObject.m
//  MakoPlayer
//
//  Created by Ran Greenberg on 5/7/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import "PlayerActivityObject.h"
#import "AFNetWorking.h"

#define ACTIVITY_OBJECT_PLAYER_ACTIVITY_DATA        @"playerActivityData"
#define ACTIVITY_OBJECT_AD_SUCCESS                  @"AD_SUCCESS"

@interface PlayerActivityObject () <NSObject>

@property (nonatomic,strong) NSMutableDictionary *playerActivityData;
@property (nonatomic, readwrite) NSInteger watchActivityCounter;
@property (nonatomic, strong) NSMutableArray *watchDatesArray;

@property (nonatomic, copy) ActivityStartBlock startBlock;
@property (nonatomic, copy) ActivityStopBlock stopBlock;
@property (nonatomic, strong) NSTimer *pollingTimer;

@property (nonatomic) BOOL isFirstTimeFetch;


@end


@implementation PlayerActivityObject


-(NSMutableArray *)watchDatesArray {
    
    if (!_watchDatesArray)
        _watchDatesArray = [[NSMutableArray alloc] init];
    
    return _watchDatesArray;
}


-(id)initWithDictionary:(NSDictionary*)dict {
    
    self = [super init];
    
    if (self) {
        
        self.playerActivityData = [[NSMutableDictionary alloc] initWithDictionary:dict];
        
    }
    return self;
}


-(NSString*)stringValueForKey:(NSString*)key {
    
    return [self.playerActivityData objectForKey:key];
}


-(void)snapDateOfWatching {
    
    NSDate *now = [NSDate date];
    [self.watchDatesArray addObject:now];
}


-(BOOL)isTimingAllowToWatch {
    
    NSDate *now = [NSDate date];
    NSInteger viewCountingPeriod = [self stringValueForKey:ACTIVITY_OBJECT_VIEW_COUNTING_PERIOD].integerValue;
    NSInteger numberOfViews = [self stringValueForKey:ACTIVITY_OBJECT_NUMBER_OF_VIEWS].integerValue;
    
    for (NSDate *watchData in self.watchDatesArray) {
        NSInteger secondsPast = ABS([now timeIntervalSinceDate:watchData]);
        
        // convert to minutes
        NSInteger minutesPast = secondsPast/60;
        
        // count the view just in case it's in the counting period, else do nothing
        if (minutesPast < viewCountingPeriod) {
            numberOfViews--;
        }
    }
    
    if (numberOfViews > 0)
        return YES;
    
    return NO;
}


-(void)startPollingWithStartBlock:(ActivityStartBlock)startBlock stopBlock:(ActivityStopBlock)stopBlock {
    
    self.startBlock = startBlock;
    self.stopBlock = stopBlock;
    
    [self fetchPlayerActivityData];
    
}


-(void)fetchPlayerActivityData {
    NSString * strUrl = [self stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_URL];
    if (!strUrl) {
        return;
    }
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:[self stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_URL]]];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET"
                                                            path:[self stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_URL]
                                                      parameters:nil];
    
    
    AFHTTPRequestOperation *currentOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [currentOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // if AD_SUCCESS found
        if ([[operation responseString] rangeOfString:ACTIVITY_OBJECT_AD_SUCCESS].location != NSNotFound) {
            
            self.startBlock(self);
            self.isFirstTimeFetch = YES;
            
        }
        else {
            klogdbg_func_arg1s([self stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_ID])
            klogdbg_func_arg1s(@"No AD_SUCCESS found - Do nothing");
            self.stopBlock(self);
        }
        
        
        [self startPollingTimer];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    [currentOperation start];
    
}



-(void)stopActivity {
    [self stopPollingTimer];
}



#pragma mark - NSCoding


- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.playerActivityData forKey:ACTIVITY_OBJECT_PLAYER_ACTIVITY_DATA];
    
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super init];
    
    if (self) {
        
        self.playerActivityData = [aDecoder decodeObjectForKey:ACTIVITY_OBJECT_PLAYER_ACTIVITY_DATA];
    }
    
    return self;
}


#pragma mark - NSTimer operations


-(void)startPollingTimer {
    [self stopPollingTimer];
    klogdbg_func
    
    NSTimeInterval pollingInterval = [self stringValueForKey:ACTIVITY_OBJECT_POLLING_INTERVAL].doubleValue;
    
    if (pollingInterval < 10.0) pollingInterval = 10.0;
    
    self.pollingTimer = [NSTimer scheduledTimerWithTimeInterval:pollingInterval target:self selector:@selector(fetchPlayerActivityData) userInfo:nil repeats:NO];
    
}

-(void)stopPollingTimer {
    klogdbg_func
    
    [self.pollingTimer invalidate];
    self.pollingTimer = nil;
}


@end
