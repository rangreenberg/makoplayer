//
//  ActivityManager.h
//  MakoPlayer
//
//  Created by Ran Greenberg on 5/7/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlayerActivityObject.h"
#import "VideoItem.h"


typedef void(^OperationCompletionBlock)(BOOL isSuccess);
typedef void(^CompletionBlock)();

@interface PlayerActivityManager : NSObject

+(PlayerActivityManager*)sharedInstance;

-(void)initWithData:(NSData*)data completion:(OperationCompletionBlock)completionBlock;
-(NSArray*)activitiesArrayForVideoItem:(id<VideoItem>)videoItem;
-(PlayerActivityObject*)activityForActivityId:(NSString*)activityIDString;

-(void)stopActivities;

@end
