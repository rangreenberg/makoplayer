//
//  ActivityManager.m
//  MakoPlayer
//
//  Created by Ran Greenberg on 5/7/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import "PlayerActivityManager.h"
#import "SBJson4.h"

#define ACTIVITIES_CONFIG_ARRAY_KEY   @"activities"


@interface PlayerActivityManager () <NSObject>

@property (nonatomic, strong) NSDictionary *activitiesDict;
@property (nonatomic, strong) NSMutableDictionary *playerActivityDict;

@end

@implementation PlayerActivityManager

+(PlayerActivityManager*) sharedInstance {
    static PlayerActivityManager *instance = nil;
    static dispatch_once_t oncePredicate;
    
    if (instance && !instance.activitiesDict)
        NSLog(@"PlayerActivityManager didn't set data!");
    
    
    dispatch_once(&oncePredicate, ^{
        instance = [[PlayerActivityManager alloc] init];
    });
    
    return instance;
    
}

-(void)initWithData:(NSData*)data completion:(OperationCompletionBlock)completionBlock {
    
    [self parseWithData:data completion:completionBlock];
    
}


-(void)parseWithData:(NSData*)data completion:(OperationCompletionBlock)completionBlock {
    SBJson4Parser *parser = [SBJson4Parser parserWithBlock:^(id item, BOOL *stop) {
        
        NSObject *itemObject = item;
        
        if ([item isKindOfClass:[NSDictionary class]]) {
            self.activitiesDict = (NSDictionary*)itemObject;
            [self parseActivitiesWithDictionary:self.activitiesDict];
            
            completionBlock(YES);
        }
    }
                                            allowMultiRoot:NO
                                           unwrapRootArray:NO
                                              errorHandler:^(NSError *error) {
                                                  klogdbg_func_arg1s(@"Faild to parse data!")
                                                  NSLog(@"%@", error);
                                                  
                                                  completionBlock(NO);
                                              }];
    [parser parse:data];
    
}

-(void)parseActivitiesWithDictionary:(NSDictionary*)dict {
    
    NSMutableDictionary *activitiesDictAns = [[NSMutableDictionary alloc] init];
    NSObject *activitiesObject = [dict objectForKey:ACTIVITIES_CONFIG_ARRAY_KEY];
    
    if ([activitiesObject isKindOfClass:[NSArray class]]) {
        NSArray *activitiesArray = (NSArray*)activitiesObject;
        
        for (NSDictionary *activityDict in activitiesArray) {
            
            PlayerActivityObject *playerActivityObject = [[PlayerActivityObject alloc] initWithDictionary:activityDict];
            NSString *activityId = [playerActivityObject stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_ID];
            if (activityId) {
                [activitiesDictAns setObject:playerActivityObject forKey:[playerActivityObject stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_ID]];
            }
        }
    }
    
    self.playerActivityDict = activitiesDictAns;
    
}

-(NSArray*)activitiesArrayForVideoItem:(id<VideoItem>)videoItem {
    
    NSMutableArray *activitiesForVideoItem = [[NSMutableArray alloc] init];
    
    for (NSString *playerActivityObjKey in self.playerActivityDict) {
        
        PlayerActivityObject *playerActivityObj = [self.playerActivityDict objectForKey:playerActivityObjKey];
        if (!playerActivityObj) continue;
        
        // check whether the current watching is less than the player activity object (key: "number_of_views")
        if ([playerActivityObj isTimingAllowToWatch]) {
            
            
            NSString *mediaType = [playerActivityObj stringValueForKey:ACTIVITY_OBJECT_MEDIA_TYPE];
            
            // default
            if ([mediaType isEqualToString:ACTIVITY_OBJECT_MEDIA_TYPE_DEFAULT]) {
                
                [activitiesForVideoItem addObject:playerActivityObj];
            }
            
            // guid
            else if ([mediaType isEqualToString:ACTIVITY_OBJECT_MEDIA_TYPE_GUID]) {
                
                if ([videoItem.guid isEqualToString:[playerActivityObj stringValueForKey:ACTIVITY_OBJECT_MEDIA_ID]]) {
                    [activitiesForVideoItem addObject:playerActivityObj];
                }
            }
            
            // url
            else if ([mediaType isEqualToString:ACTIVITY_OBJECT_MEDIA_TYPE_URL]) {
                
                if ([videoItem.relativeLink hasPrefix:[playerActivityObj stringValueForKey:ACTIVITY_OBJECT_MEDIA_ID]]) {
                    [activitiesForVideoItem addObject:playerActivityObj];
                }
            }
            
            // channel
            else if ([mediaType isEqualToString:ACTIVITY_OBJECT_MEDIA_TYPE_CHANNEL]) {
                
                if ([videoItem.channelGuid isEqualToString:[playerActivityObj stringValueForKey:ACTIVITY_OBJECT_MEDIA_ID]]) {
                    [activitiesForVideoItem addObject:playerActivityObj];
                }
            }
        }
    }
    
    klogdbg_func_arg1s(activitiesForVideoItem)
    return activitiesForVideoItem;
}


-(PlayerActivityObject*)activityForActivityId:(NSString*)activityIDString {
    
    for (NSString *playerActivityObjKey in self.playerActivityDict) {
        
        PlayerActivityObject *playerActivityObj = [self.playerActivityDict objectForKey:playerActivityObjKey];
        if (!playerActivityObj) continue;
        
        if ([[playerActivityObj stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_ID] isEqualToString:activityIDString])
            return playerActivityObj;
    }
    
    return nil;
}


-(void)stopActivities {
    
    for (NSString *activityIdKey in self.playerActivityDict) {
        id activityObject = [self.playerActivityDict objectForKey:activityIdKey];
        
        if ([activityObject isKindOfClass:[PlayerActivityObject class]])
            [(PlayerActivityObject*)activityObject stopActivity];
    }
}


@end
