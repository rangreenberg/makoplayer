//
//  PlayerActivityButton.m
//  MakoPlayer
//
//  Created by Ran Greenberg on 5/11/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import "PlayerActivityButton.h"


@interface PlayerActivityButton () <NSObject>

@property (nonatomic, strong, readwrite) NSString *activityID;

@end


@implementation PlayerActivityButton


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithActivityID:(NSString*)activityID
{
    self = [super init];
    if (self) {
        self.activityID = activityID;
    }
    return self;
}




@end
