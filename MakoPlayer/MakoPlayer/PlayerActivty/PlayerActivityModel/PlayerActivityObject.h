//
//  ActivityObject.h
//  MakoPlayer
//
//  Created by Ran Greenberg on 5/7/14.
//  Copyright (c) 2014 mako. All rights reserved.
//
#import <Foundation/Foundation.h>

#define ACTIVITY_OBJECT_ACTIVITY_ID                             @"activity_id"
#define ACTIVITY_OBJECT_ACTIVITY_URL                            @"activity_url"
#define ACTIVITY_OBJECT_MEDIA_ID                                @"media_id"
#define ACTIVITY_OBJECT_MEDIA_TYPE                              @"media_type"
#define ACTIVITY_OBJECT_ACTIVITY_WIDTH                          @"activity_width"
#define ACTIVITY_OBJECT_ACTIVITY_HEIGHT                         @"activity_height"
#define ACTIVITY_OBJECT_ACTIVITY_X                              @"activity_x"
#define ACTIVITY_OBJECT_ACTIVITY_Y                              @"activity_y"
#define ACTIVITY_OBJECT_ACTIVITY_DOCK_VERTICAL                  @"activity_dock_vertical"
#define ACTIVITY_OBJECT_ACTIVITY_DOCK_HORIZONTAL                @"activity_dock_horizontal"
//#define ACTIVITY_OBJECT_ACTIVITY_START_TIME                     @"activity_start_time"
//#define ACTIVITY_OBJECT_ACTIVITY_DURATION                       @"activity_duration"
#define ACTIVITY_OBJECT_ICON_URL                                @"icon_url"
#define ACTIVITY_OBJECT_ICON_MINIMIZED_URL                      @"icon_minimized_url"
#define ACTIVITY_OBJECT_CLOSE_ICON_URL                          @"close_icon_url"
#define ACTIVITY_OBJECT_MINIMIZE_ICON_URL                       @"minimize_icon_url"
#define ACTIVITY_OBJECT_ICON_WIDTH                              @"icon_width"
#define ACTIVITY_OBJECT_ICON_HEIGHT                             @"icon_height"
#define ACTIVITY_OBJECT_ICON_X                                  @"icon_x"
#define ACTIVITY_OBJECT_ICON_Y                                  @"icon_y"
#define ACTIVITY_OBJECT_ICON_DOCK_VERTICAL                      @"icon_dock_vertical"
#define ACTIVITY_OBJECT_ICON_DOCK_HORIZONTAL                    @"icon_dock_horizontal"
#define ACTIVITY_OBJECT_NUMBER_OF_VIEWS                         @"number_of_views"
#define ACTIVITY_OBJECT_VIEW_COUNTING_PERIOD                    @"view_counting_period"
#define ACTIVITY_OBJECT_EFFECT                                  @"effect"
#define ACTIVITY_OBJECT_POLLING_INTERVAL                        @"polling_interval"
#define ACTIVITY_OBJECT_IS_INITIALLY_MINIMIZED                  @"is_initially_minimized"

// options
#define ACTIVITY_OBJECT_MEDIA_TYPE_GUID                         @"guid"
#define ACTIVITY_OBJECT_MEDIA_TYPE_URL                          @"url"
#define ACTIVITY_OBJECT_MEDIA_TYPE_CHANNEL                      @"channel"
#define ACTIVITY_OBJECT_MEDIA_TYPE_DEFAULT                      @"default"

//effects
#define ACTIVITY_OBJECT_EFFECT_NONE                             @"none"
#define ACTIVITY_OBJECT_EFFECT_FADE                             @"fade"
#define ACTIVITY_OBJECT_EFFECT_SLIDE                            @"slide"
#define ACTIVITY_OBJECT_EFFECT_GENIE                            @"genie"



@interface PlayerActivityObject : NSObject <NSCoding>

typedef void(^ActivityStartBlock)(PlayerActivityObject *activityObject);
typedef void(^ActivityStopBlock)(PlayerActivityObject *activityObject);

@property (nonatomic, readonly) NSInteger watchActivityCounter;
@property (nonatomic, strong) UIImage *iconMinimizeImage;
@property (nonatomic, strong) UIImage *iconOpenImage;

-(id)initWithDictionary:(NSDictionary*)dict;
-(NSString*)stringValueForKey:(NSString*)key;
-(void)snapDateOfWatching;
-(BOOL)isTimingAllowToWatch;
-(void)startPollingWithStartBlock:(ActivityStartBlock)startBlock stopBlock:(ActivityStopBlock)stopBlock;
-(void)stopActivity;

@end
