//
//  PlayerActivityButton.h
//  MakoPlayer
//
//  Created by Ran Greenberg on 5/11/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayerActivityButton : UIButton

@property (nonatomic, strong, readonly) NSString *activityID;


- (id)initWithActivityID:(NSString*)activityID;
@end
