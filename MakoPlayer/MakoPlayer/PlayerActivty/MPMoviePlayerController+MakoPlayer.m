//
//  MPMoviePlayerController+MakoPlayer.m
//  MakoPlayer
//
//  Created by Ran Greenberg on 5/1/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import "MPMoviePlayerController+MakoPlayer.h"
#import "UIView+Genie.h"
#import <objc/runtime.h>
#import "PlayerActivityManager.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "PlayerActivityButton.h"

#define ICON_DEFAULT_HEIGHT         @"57"
#define ICON_DEFAULT_WIDTH          @"57"

#define ACTIVITY_DEFAULT_HEIGHT     @"100"
#define ACTIVITY_DEFAULT_WIDTH      @"100"

#define IS_IPHONE_5 ( [ [ UIScreen mainScreen ] bounds ].size.height == 568 )

typedef void (^OperationCompletionWithImageAndSuccess)(UIImage *image, BOOL isSuccess);


@interface MPMoviePlayerController () <UIWebViewDelegate>

//@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, strong) NSNumber *isWebViewPresented;
@property (nonatomic, strong) NSDictionary *activityIconsDictionary;
@property (nonatomic, strong) PlayerActivityObject *currentActiveActivity;
@property (nonatomic, strong) PlayerActivityButton *currentButtonShown;
@property (nonatomic, strong) NSMutableDictionary *activityIdToWebView;
@property (nonatomic, strong) NSMutableDictionary *activityIdToPlayerActivityButton;
@property (nonatomic, strong) NSString *contentId;
@property (nonatomic, strong) NSString *contentUrl;
@property (nonatomic, strong) id<VideoItem> videoItem;

@end


@implementation MPMoviePlayerController (MakoPlayer)


-(NSMutableDictionary *)activityIdToWebView {
    
    NSMutableDictionary *dict = objc_getAssociatedObject(self, @"activityIdToWebView");
    if (!dict) {
        dict = [[NSMutableDictionary alloc] init];
        objc_setAssociatedObject(self, @"activityIdToWebView", dict, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    
    return objc_getAssociatedObject(self, @"activityIdToWebView");
}


- (void)setActivityIdToWebView:(NSMutableDictionary*)activityIdToWebView {
    objc_setAssociatedObject(self, @"activityIdToWebView", activityIdToWebView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
}


-(NSMutableDictionary *)activityIdToPlayerActivityButton {
    
    NSMutableDictionary *dict = objc_getAssociatedObject(self, @"activityIdToPlayerActivityButton");
    if (!dict) {
        dict = [[NSMutableDictionary alloc] init];
        objc_setAssociatedObject(self, @"activityIdToPlayerActivityButton", dict, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    
    return objc_getAssociatedObject(self, @"activityIdToPlayerActivityButton");
}


- (void)setActivityIdToPlayerActivityButton:(NSMutableDictionary*)activityIdToPlayerActivityButton {
    objc_setAssociatedObject(self, @"activityIdToPlayerActivityButton", activityIdToPlayerActivityButton, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
}


-(PlayerActivityObject *)currentActiveActivity {
    
    return objc_getAssociatedObject(self, @"currentActiveActivity");
}


- (void)setCurrentActiveActivity:(PlayerActivityObject*)currentActiveActivity {
    objc_setAssociatedObject(self, @"currentActiveActivity", currentActiveActivity, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
}


-(PlayerActivityButton *)currentButtonShown {
    
    return objc_getAssociatedObject(self, @"currentButtonShown");
}


- (void)setCurrentButtonShown:(PlayerActivityButton*)currentButtonShown {
    objc_setAssociatedObject(self, @"currentButtonShown", currentButtonShown, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
}


-(NSDictionary *)activityIconsDictionary {
    
    return objc_getAssociatedObject(self, @"activityIconsDictionary");
}


- (void)setActivityIconsDictionary:(NSDictionary*)activityIconsDictionary {
    objc_setAssociatedObject(self, @"activityIconsDictionary", activityIconsDictionary, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
}


- (void) setIsWebViewPresented:(NSNumber*) property {
    objc_setAssociatedObject(self, @"isWebViewPresented", property, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


-(NSNumber *)isWebViewPresented {
    
    return objc_getAssociatedObject(self, @"isWebViewPresented");
}


-(UIButton *)button {
    
    return objc_getAssociatedObject(self, @"button");
}


- (void)setButton:(UIButton*)button {
    objc_setAssociatedObject(self, @"button", button, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
}

-(id<VideoItem>)videoItem {
    
    return objc_getAssociatedObject(self, @"videoItem");
}


- (void)setVideoItem:(id<VideoItem>)videoItem {
    objc_setAssociatedObject(self, @"videoItem", videoItem, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
}


-(void)startActivitiesOnPlayerWithVideoItem:(id<VideoItem>)videoItem withActivitiesPath:(NSString*)path {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(doneButtonClick:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:nil];
    
    //    [self watcher];
    
    // init Docking dictionary
    self.activityIconsDictionary = @{
                                     @"tl":[[NSMutableArray alloc] init],
                                     @"tc":[[NSMutableArray alloc] init],
                                     @"tr":[[NSMutableArray alloc] init],
                                     @"ml":[[NSMutableArray alloc] init],
                                     @"mc":[[NSMutableArray alloc] init],
                                     @"mr":[[NSMutableArray alloc] init],
                                     @"bl":[[NSMutableArray alloc] init],
                                     @"bc":[[NSMutableArray alloc] init],
                                     @"br":[[NSMutableArray alloc] init]
                                     };
    self.videoItem = videoItem;
    

    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:path]];

    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET"
                                                            path:path
                                                      parameters:nil];
    
    
    AFHTTPRequestOperation *currentOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];

    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    [currentOperation setCacheResponseBlock:^NSCachedURLResponse *(NSURLConnection *connection, NSCachedURLResponse *cachedResponse) {
        return nil;
    }];

    [currentOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // PlayerActivityManager parse the activities JSON with SBJSON
        // which is asynchronous therefor we need to pass a completion block
        [[PlayerActivityManager sharedInstance] initWithData:operation.responseData completion:^(BOOL isSuccess){
            
            if (isSuccess) {
                NSArray *activitiesArrayForVideoItemArray = [[PlayerActivityManager sharedInstance] activitiesArrayForVideoItem:videoItem];
                
                if (activitiesArrayForVideoItemArray && [activitiesArrayForVideoItemArray count] > 0)
                    [self proccessActivities:activitiesArrayForVideoItemArray forVideoItem:videoItem];
            }
            
            else {
                NSLog(@"Can't get player activities FAILED to parse activities JSON");
            }
        }];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    [currentOperation start];
    
}


-(void)doneButtonClick:(NSNotification*)notification {
    NSNumber *reason = [notification.userInfo objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    klogdbg_func_arg1u(reason.intValue)
    
    // MPMoviePlayerController done pressed - stop
    if ([reason intValue] == MPMovieFinishReasonUserExited) {
        [self stopActivities];
        
    }
}

-(void)stopActivitiesOnPlayer {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:nil];
    [self stop];
    [self stopActivities];
    
}

//-(void)watcher {
//    NSString *str = [NSString stringWithFormat:@"%f",self.currentPlaybackTime];
//    klogdbg_func_arg1s(str)
//
//    if (isnan(self.currentPlaybackTime))
//    {
//
//    }
//
//    else
//    {
//        [self performSelector:@selector(watcher) withObject:nil afterDelay:0.5];//to update the value each 0.5 seconds
//    }
//}

-(void)dealloc {
    klogdbg_func
}

-(void)stopActivities {
    klogdbg_func
    
    [[PlayerActivityManager sharedInstance] stopActivities];
}


-(void)proccessActivities:(NSArray*)activitiesArray forVideoItem:(id<VideoItem>)videoItem {
    
    for (int i = 0; i<[activitiesArray count]; i++) {
        PlayerActivityObject *playerActivity = activitiesArray[i];
        
        [playerActivity startPollingWithStartBlock:^(PlayerActivityObject *activityObject) {
            
            PlayerActivityButton *activityObjectButton = [self.activityIdToPlayerActivityButton objectForKey:[activityObject stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_ID]];
            
            if (activityObjectButton) {
                klogdbg_func_arg1s(activityObjectButton.activityID)
                activityObjectButton.hidden = NO;
            }
            
            else {
                
                // for each activity, add web view and save it to activityId -> activity web view dictionary
                UIWebView *webView = [self createWebViewforActivity:playerActivity];
                [self.activityIdToWebView setObject:webView forKey:[playerActivity stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_ID]];
                [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[playerActivity stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_URL]]]];
                
                webView.scrollView.scrollEnabled = NO;
                
                // activity is initially minimized
                if ([[activityObject stringValueForKey:ACTIVITY_OBJECT_IS_INITIALLY_MINIMIZED] isEqualToString:@"true"]) {
                    
                    [self proccessActivityIconsAndAddToPlayerWithActivityObject:activityObject completion:nil];
                    
                }
                
                // activity is initially open with reference to timer
                else {
                    [self proccessActivityIconsAndAddToPlayerWithActivityObject:activityObject completion:^{
                        
                        PlayerActivityButton *activityButton = [self.activityIdToPlayerActivityButton objectForKey:[activityObject stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_ID]];
                        
                        [self buttonPressed:activityButton];
                    }];
                }
            }
        }
                                         stopBlock:^(PlayerActivityObject *activityObject) {
                                             klogdbg_func
                                             PlayerActivityButton *activityObjectButton = [self.activityIdToPlayerActivityButton objectForKey:[activityObject stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_ID]];
                                             if (activityObjectButton)
                                                 activityObjectButton.hidden = YES;
                                         }];
    }
}


-(void)proccessActivityIconsAndAddToPlayerWithActivityObject:(PlayerActivityObject*)activityObject completion:(CompletionBlock)completionBlock {
    
    // load the activity icon (open) - if icon (open) isn't exist, do nothing,
    // this is blocking, activity will show if and only if icon (open) is exist
    NSString *iconUrl = [activityObject stringValueForKey:ACTIVITY_OBJECT_ICON_URL];
    if (!iconUrl) {
        return;
    }
    [self processIconWithUrlString:[activityObject stringValueForKey:ACTIVITY_OBJECT_ICON_URL] completionBlockWithImage:^(UIImage *iconOpen, BOOL isSuccess) {
        
        if (isSuccess) {
            activityObject.iconOpenImage = iconOpen;
            
            NSString *iconMinimizedUrl = [activityObject stringValueForKey:ACTIVITY_OBJECT_ICON_MINIMIZED_URL];
            
            // if icon minimised is non empty
            if (iconMinimizedUrl && iconMinimizedUrl.length > 0) {
                [self processIconWithUrlString:[activityObject stringValueForKey:ACTIVITY_OBJECT_ICON_MINIMIZED_URL] completionBlockWithImage:^(UIImage *minimizedImage, BOOL isSuccess) {
                    
                    // if success to load the icon minimised
                    if (isSuccess && minimizedImage) {
                        activityObject.iconMinimizeImage = minimizedImage;
                    }
                    
                    // if faild to load the icon minimised set it to the activity icon (open)
                    else {
                        activityObject.iconMinimizeImage = iconOpen;
                    }
                    
                    // add activity button
                    [self addButtonWithActivityObject:activityObject image:activityObject.iconMinimizeImage];
                    if (completionBlock)
                        completionBlock();
                }];
            }
            
            // add activity button - the icon minimised isn't blocking,
            // therefore activity icon will show on screen anyway (icon minimised will be the icon open)
            else {
                activityObject.iconMinimizeImage = iconOpen;
                [self addButtonWithActivityObject:activityObject image:iconOpen];
                if (completionBlock)
                    completionBlock();
            }
        }
    }];
}


-(UIWebView *)createWebViewforActivity:(PlayerActivityObject *)playerActivity {
    
    CGRect frame;
    
    NSString *activityWidth = [playerActivity stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_WIDTH];
    NSString *activityHeight = [playerActivity stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_HEIGHT];
    
    NSString *activityX = [playerActivity stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_X];
    NSString *activityY = [playerActivity stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_Y];
    
    if (!activityHeight) activityHeight = ACTIVITY_DEFAULT_HEIGHT;
    if (!activityWidth) activityWidth = ACTIVITY_DEFAULT_WIDTH;
    
    NSInteger heightInteger = 0;
    NSInteger widthInteger = 0;
    
    
    if ([activityWidth rangeOfString:@"px"].location != NSNotFound && [activityHeight rangeOfString:@"px"].location != NSNotFound) {
        heightInteger = activityHeight.integerValue;
        widthInteger = activityWidth.integerValue;
    } else if  ([activityWidth rangeOfString:@"%"].location != NSNotFound && [activityHeight rangeOfString:@"%"].location != NSNotFound) {
        NSInteger x = [ [ UIScreen mainScreen ] bounds ].size.height;
        heightInteger = (activityHeight.doubleValue/100.0) * [ [ UIScreen mainScreen ] bounds ].size.width;
        widthInteger = (activityWidth.doubleValue/100.0) * [ [ UIScreen mainScreen ] bounds ].size.height;
    }
    
    NSInteger xInteger = 0;
    NSInteger yInteger = 0;
    
    // in case icon_x and icon_y are define and valid
    if (activityX && activityY && activityX.length > 0 && activityY.length > 0) {
        
        // in case the x and y are pixels (px)
        if ([activityX rangeOfString:@"px"].location != NSNotFound && [activityY rangeOfString:@"px"].location != NSNotFound) {
            xInteger = activityX.integerValue;
            yInteger = activityY.integerValue;
        }
        
        // in case the x and y are precentage (%)
        else if ([activityX rangeOfString:@"%"].location != NSNotFound && [activityY rangeOfString:@"%"].location != NSNotFound) {
           xInteger = (activityX.doubleValue/100.0) * [ [ UIScreen mainScreen ] bounds ].size.height;
            yInteger = (activityY.doubleValue/100.0) * [ [ UIScreen mainScreen ] bounds ].size.width;
        }
        
        //        [self.view addSubview:minimizedButton];
    }
    frame = CGRectMake(xInteger, yInteger, widthInteger, heightInteger);
    
    
    //Set a default 100x100 size in case the activity has no size
    if (frame.size.width == 0) {
        frame.size.width = 100;
    }
    
    if (frame.size.height == 0) {
        frame.size.height = 100;
    }
    
    UIWebView *webView = [[UIWebView alloc]initWithFrame:frame];
    webView.opaque = NO;
    webView.backgroundColor = [UIColor clearColor];
    webView.delegate = self;
    webView.hidden = YES;
    [self.view addSubview:webView];
    return webView;
}

-(void)processIconWithUrlString:(NSString*)urlString completionBlockWithImage:(OperationCompletionWithImageAndSuccess)completionBlockWithImageAndSuccess {
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:urlString]];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET"
                                                            path:urlString
                                                      parameters:nil];
    
    
    AFHTTPRequestOperation *currentOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    
    [currentOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        completionBlockWithImageAndSuccess([UIImage imageWithData:operation.responseData], YES);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        completionBlockWithImageAndSuccess(nil, NO);
    }];
    
    [currentOperation start];
    
}


-(void)addButtonWithActivityObject:(PlayerActivityObject*)activityObject image:(UIImage*)image {
    klogdbg_func_arg1s([activityObject stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_ID])
    
    PlayerActivityButton *minimizedButton = [[PlayerActivityButton alloc] initWithActivityID:[activityObject stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_ID]];
    [self.activityIdToPlayerActivityButton setObject:minimizedButton forKey:[activityObject stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_ID]];
    UIImage *buttonImage = [[activityObject stringValueForKey:ACTIVITY_OBJECT_IS_INITIALLY_MINIMIZED] isEqualToString:@"true"] ? activityObject.iconMinimizeImage : activityObject.iconOpenImage;
    [minimizedButton setImage:buttonImage forState:UIControlStateNormal];
    [minimizedButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect frame;
    
    NSString *iconX = [activityObject stringValueForKey:ACTIVITY_OBJECT_ICON_X];
    NSString *iconY = [activityObject stringValueForKey:ACTIVITY_OBJECT_ICON_X];
    
    NSString *iconWidth = [activityObject stringValueForKey:ACTIVITY_OBJECT_ICON_WIDTH];
    NSString *iconHeight = [activityObject stringValueForKey:ACTIVITY_OBJECT_ICON_HEIGHT];
    
    NSString *iconDockingVertical = [activityObject stringValueForKey:ACTIVITY_OBJECT_ICON_DOCK_VERTICAL];
    NSString *iconDockingHorizontal = [activityObject stringValueForKey:ACTIVITY_OBJECT_ICON_DOCK_HORIZONTAL];
    
    if (!iconHeight) iconHeight = ICON_DEFAULT_HEIGHT;
    if (!iconWidth) iconWidth = ICON_DEFAULT_WIDTH;
    
    NSInteger heightInteger = iconHeight.integerValue;
    NSInteger widthInteger = iconWidth.integerValue;
    
    // in case icon_x and icon_y are define and valid
    if (iconX && iconY && iconX.length > 0 && iconY.length > 0) {
        
        // in case the x and y are pixels (px)
        if ([iconX rangeOfString:@"px"].location != NSNotFound && [iconY rangeOfString:@"px"].location != NSNotFound) {
            NSInteger xInteger = iconX.integerValue;
            NSInteger yInteger = iconY.integerValue;
            frame = CGRectMake(xInteger, yInteger, widthInteger, heightInteger);
        }
        
        // in case the x and y are precentage (%)
        else if ([iconX rangeOfString:@"%"].location != NSNotFound && [iconY rangeOfString:@"%"].location != NSNotFound) {
            NSInteger xInteger = (iconX.doubleValue/100.0) * [ [ UIScreen mainScreen ] bounds ].size.height;
            NSInteger yInteger = (iconY.doubleValue/100.0) * [ [ UIScreen mainScreen ] bounds ].size.width;
            frame = CGRectMake(xInteger, yInteger, widthInteger, heightInteger);
        }
        
        //        [self.view addSubview:minimizedButton];
    }
    
    // in case icon_dock_vertical and icon_dock_horizontal are define and valid
    else if (iconDockingVertical && iconDockingVertical && iconDockingHorizontal.length > 0 && iconDockingHorizontal.length > 0) {
        [self addViewWithDockingHorizontal:iconDockingHorizontal vertical:iconDockingVertical view:minimizedButton];
    }
    
    minimizedButton.frame = frame;
    
    [self addViewWithDockingHorizontal:iconDockingHorizontal vertical:iconDockingVertical view:minimizedButton];
}


-(void)buttonPressed:(PlayerActivityButton*)sender {
    [self buttonPressed:sender completion:nil];
}


-(void)buttonPressed:(PlayerActivityButton*)sender completion:(CompletionBlock)completionBlock {
    
    PlayerActivityObject *playerActivityPressedObj = [[PlayerActivityManager sharedInstance] activityForActivityId:sender.activityID];
    
    // open
    if (self.isWebViewPresented.intValue == 0) {
        
        // open pressed button activity
        [self openActivity:sender completion:^{
            self.currentButtonShown = sender;
            self.currentActiveActivity = playerActivityPressedObj;
            self.isWebViewPresented = [NSNumber numberWithInt:1];
            [sender setImage:playerActivityPressedObj.iconOpenImage forState:UIControlStateNormal];
            [self reportEventwithAction:@"OpenActivityWV"];
            if (completionBlock)
                completionBlock();
        }];
        
        

    }
    
    // close + reopen
    else {
        
        if (self.currentActiveActivity != playerActivityPressedObj) {
            
            // close current shown activity
            [self closeActivity:self.currentButtonShown completion:^{
                [self reportEventwithAction:@"CloseActivityWV"];
                [self.currentButtonShown setImage:self.currentActiveActivity.iconMinimizeImage forState:UIControlStateNormal];
                self.currentButtonShown = nil;
                self.currentActiveActivity = nil;
                
                // open pressed button activity
                [self openActivity:sender completion:^{
                    self.currentButtonShown = sender;
                    self.isWebViewPresented = [NSNumber numberWithInt:0];
                    
                    self.currentActiveActivity = playerActivityPressedObj;
                    self.isWebViewPresented = [NSNumber numberWithInt:1];
                    
                    [sender setImage:playerActivityPressedObj.iconOpenImage forState:UIControlStateNormal];
                    [self reportEventwithAction:@"OpenActivityWV"];
                    if (completionBlock)
                        completionBlock();
                }];
            }];
        }
        
        else if (self.currentActiveActivity == playerActivityPressedObj) {
            
            // close current shown activity
            [self closeActivity:self.currentButtonShown completion:^{
                
                [self.currentButtonShown setImage:playerActivityPressedObj.iconMinimizeImage forState:UIControlStateNormal];
                self.currentActiveActivity = nil;
                self.currentButtonShown = nil;
                self.isWebViewPresented = [NSNumber numberWithInt:0];
                
                if (completionBlock)
                    completionBlock();
            }];
            
            [self reportEventwithAction:@"CloseActivityWV"];
        }
    }
}

-(void)reportEventwithAction:(NSString*)action {
//    // report analytics event that user shared a video
//    NSString *contentURL = [self.videoItem relativeLink];
//    NSString *videoURL = [self.contentURL absoluteString];
//    NSString *contentID = self.videoItem.guid;
//    NSString *WebviewFileURL = [self.currentActiveActivity stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_URL];
//    
//    NSString * appVersion = [Util appVersion];
//    NSString * params = [NSString stringWithFormat:
//                         @"iphone|makoTV|v%@|contentId=%@|contentURL=%@|videoURL=%@|WebviewFileURL=%@]",
//                         appVersion,
//                         contentID,
//                         contentURL,
//                         videoURL,
//                         WebviewFileURL];
//    
//    // report analytics
//    [[MMAnalytics shared] trackEvent:@"PlayerWebViewActivity" withAction:action withParam: params];

}


//Open activity with the chosen animation
-(void)openActivity:(PlayerActivityButton*)button completion:(CompletionBlock)completionBlock {
    
    PlayerActivityObject *playerActivityPressedObj = [[PlayerActivityManager sharedInstance] activityForActivityId:button.activityID];
    NSString *effectString = [playerActivityPressedObj stringValueForKey:ACTIVITY_OBJECT_EFFECT];
    
    //If there is no effect use fade as default
    if (!effectString) {
        effectString = @"fade";
    }
    
    UIWebView *currentWebView = [self.activityIdToWebView objectForKey:[playerActivityPressedObj stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_ID]];
    
    if (!currentWebView) return;
    currentWebView.hidden = NO;
    
    //Genie
    if ([effectString caseInsensitiveCompare:ACTIVITY_OBJECT_EFFECT_GENIE] == NSOrderedSame) {
        [self genieAnimation:button
                  completion:completionBlock
                 shuouldOpen:YES
              currentWebView:currentWebView];
        
        //Fade in
    } else if ([effectString caseInsensitiveCompare:ACTIVITY_OBJECT_EFFECT_FADE] == NSOrderedSame) {
        
        
        [self fadeAnimation:button
                 completion:completionBlock
                shuouldOpen:YES
             currentWebView:currentWebView];
        //None
        
    } else if ([effectString caseInsensitiveCompare:ACTIVITY_OBJECT_EFFECT_NONE] == NSOrderedSame) {
        
        [self noAnimation:button
               completion:completionBlock
              shuouldOpen:YES
           currentWebView:currentWebView];
        
        //slide
    } else if ([effectString caseInsensitiveCompare:ACTIVITY_OBJECT_EFFECT_SLIDE] == NSOrderedSame) {
        
        [self slideAnimation:button
                  completion:completionBlock
                 shuouldOpen:YES
              currentWebView:currentWebView];
    } else {
        [self fadeAnimation:button
                 completion:completionBlock
                shuouldOpen:YES
             currentWebView:currentWebView];
    }
}


//Close activity with the chosen animation
-(void)closeActivity:(PlayerActivityButton*)button completion:(CompletionBlock)completionBlock {
    
    PlayerActivityObject *playerActivityPressedObj = [[PlayerActivityManager sharedInstance] activityForActivityId:button.activityID];
    NSString *effectString = [playerActivityPressedObj stringValueForKey:ACTIVITY_OBJECT_EFFECT];
    
    //If there is no effect use fade as default
    if (!effectString) {
        effectString = @"fade";
    }
    
    UIWebView *currentWebView = [self.activityIdToWebView objectForKey:[playerActivityPressedObj stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_ID]];
    
    if (!currentWebView) return;
    
    //Genie
    if ([effectString caseInsensitiveCompare:ACTIVITY_OBJECT_EFFECT_GENIE] == NSOrderedSame) {
        
        [self genieAnimation:button
                  completion:completionBlock
                 shuouldOpen:NO
              currentWebView:currentWebView];
        
        //Fade out
    } else if ([effectString caseInsensitiveCompare:ACTIVITY_OBJECT_EFFECT_FADE] == NSOrderedSame) {
        
        [self fadeAnimation:button
                 completion:completionBlock
                shuouldOpen:NO
             currentWebView:currentWebView];
        
        //None
    } else if ([effectString caseInsensitiveCompare:ACTIVITY_OBJECT_EFFECT_NONE] == NSOrderedSame) {
        
        [self noAnimation:button
               completion:completionBlock
              shuouldOpen:NO
           currentWebView:currentWebView];
        
        //slide
    } else if ([effectString caseInsensitiveCompare:ACTIVITY_OBJECT_EFFECT_SLIDE] == NSOrderedSame) {
        
        [self slideAnimation:button
                  completion:completionBlock
                 shuouldOpen:NO
              currentWebView:currentWebView];
    } else {
        [self fadeAnimation:button
                 completion:completionBlock
                shuouldOpen:NO
             currentWebView:currentWebView];
    }
}


//Genie animation
-(void)genieAnimation:(PlayerActivityButton *)button
           completion:(CompletionBlock)completionBlock
          shuouldOpen:(BOOL)shouldOpen
       currentWebView:(UIWebView *)currentWebView {
    
    BCRectEdge edge;
    CGRect buttonRect = button.frame;
    
    if (buttonRect.origin.y == 0) {
        edge = BCRectEdgeBottom;
    } else if (buttonRect.origin.y + buttonRect.size.height == self.view.bounds.size.height) {
        edge = BCRectEdgeTop;
    } else if (buttonRect.origin.x <= self.view.bounds.size.width / 2) {
        edge = BCRectEdgeRight;
    } else {
        edge = BCRectEdgeLeft;
    }
    
    if (shouldOpen) {
        [currentWebView genieOutTransitionWithDuration:0.5
                                             startRect:button.frame
                                             startEdge:edge
                                            completion:^{
                                                
                                                if (completionBlock) {
                                                    completionBlock();
                                                    [self addMinimizeAndeCloseIcons];
                                                }
                                            }];
    } else {
        [currentWebView genieInTransitionWithDuration:0.5
                                      destinationRect:button.frame
                                      destinationEdge:edge
                                           completion:^{
                                               
                                               currentWebView.hidden = YES;
                                               if (completionBlock) {
                                                   completionBlock();
                                               }
                                           }];
    }
}


//Slide animation
-(void)slideAnimation:(PlayerActivityButton *)button
           completion:(CompletionBlock)completionBlock
          shuouldOpen:(BOOL)shouldOpen
       currentWebView:(UIWebView *)currentWebView {
    
    if (shouldOpen) {
        CGPoint webViewCenter = currentWebView.center;
        CGPoint buttonCenter = button.center;
        currentWebView.center = buttonCenter;
        currentWebView.alpha = 0;
        
        [UIView animateWithDuration:0.5
                         animations:^{
                             //currentWebView.frame = webViewRect;
                             currentWebView.center = webViewCenter;
                             currentWebView.alpha = 1;
                         }
                         completion:^ (BOOL completed) {
                             if (completionBlock) {
                                 completionBlock();
                                 [self addMinimizeAndeCloseIcons];
                             }
                         }];
    } else {
        CGPoint webViewCenter = currentWebView.center;
        CGPoint buttonCenter = button.center;
        
        [UIView animateWithDuration:0.5
                         animations:^{
                             
                             currentWebView.center = buttonCenter;
                             currentWebView.alpha = 0;
                         }
                         completion:^ (BOOL completed) {
                             if (completionBlock) {
                                 completionBlock();
                                 [self addMinimizeAndeCloseIcons];
                                 currentWebView.hidden = YES;
                                 currentWebView.center = webViewCenter;
                                 currentWebView.alpha = 1;
                                 //currentWebView.frame = webViewRect;
                             }
                         }];
    }
}


//No animation
-(void)noAnimation:(PlayerActivityButton *)button
        completion:(CompletionBlock)completionBlock
       shuouldOpen:(BOOL)shouldOpen
    currentWebView:(UIWebView *)currentWebView {
    
    if (shouldOpen) {
        [UIView animateWithDuration:0
                         animations:^{
                         }
                         completion:^ (BOOL completed) {
                             if (completionBlock) {
                                 completionBlock();
                                 [self addMinimizeAndeCloseIcons];
                             }
                         }];
    } else {
        [UIView animateWithDuration:0 animations:^{
            currentWebView.hidden = YES;
        }
                         completion:^ (BOOL completed) {
                             if (completionBlock) {
                                 completionBlock();
                                 [self addMinimizeAndeCloseIcons];
                             }
                         }];
    }
}


//Fade animation
-(void)fadeAnimation:(PlayerActivityButton *)button
          completion:(CompletionBlock)completionBlock
         shuouldOpen:(BOOL)shouldOpen
      currentWebView:(UIWebView *)currentWebView {
    
    if (shouldOpen) {
        currentWebView.alpha = 0;
        
        [UIView animateWithDuration:0.5
                         animations:^{
                             currentWebView.alpha = 1;
                         }
                         completion:^ (BOOL completed) {
                             if (completionBlock) {
                                 completionBlock();
                                 [self addMinimizeAndeCloseIcons];
                             }
                         }];
    } else {
        [UIView animateWithDuration:0.5
                         animations:^{
                             currentWebView.alpha = 0;
                         } completion:^ (BOOL completed) {
                             if (completionBlock) {
                                 completionBlock();
                                 currentWebView.hidden = YES;
                             }
                         }];
    }
}


-(void)addMinimizeAndeCloseIcons {
    NSString *minimizeIconUrlString = [self.currentActiveActivity stringValueForKey:ACTIVITY_OBJECT_MINIMIZE_ICON_URL];
    NSString *closeIconUrlString = [self.currentActiveActivity stringValueForKey:ACTIVITY_OBJECT_CLOSE_ICON_URL];
    
    UIWebView *currentWebView = [self.activityIdToWebView objectForKey:[self.currentActiveActivity stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_ID]];
    
    if (!currentWebView) return;
    
    if (closeIconUrlString) {
        [self processIconWithUrlString:closeIconUrlString completionBlockWithImage:^(UIImage *image, BOOL isSuccess) {
            
            if (isSuccess) {
                PlayerActivityButton *closeBtn = [[PlayerActivityButton alloc] initWithActivityID:
                                                  [self.currentActiveActivity stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_ID]];
                
                closeBtn.frame = CGRectMake(currentWebView.frame.size.width - image.size.width,
                                            0,
                                            image.size.width,
                                            image.size.height);
                
                [closeBtn addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
                
                [closeBtn setImage:image forState:UIControlStateNormal];
                [currentWebView addSubview:closeBtn];
            }
            
        }];
    }
    
    if (minimizeIconUrlString) {
        [self processIconWithUrlString:minimizeIconUrlString completionBlockWithImage:^(UIImage *image, BOOL isSuccess) {
            
            if (isSuccess) {
                NSInteger x = currentWebView.frame.size.width - image.size.width;
                if (closeIconUrlString) {
                    x -= 30;
                }
                PlayerActivityButton *minimizeBtn = [[PlayerActivityButton alloc] initWithActivityID:
                                                     [self.currentActiveActivity stringValueForKey:ACTIVITY_OBJECT_ACTIVITY_ID]];
                minimizeBtn.frame = CGRectMake(x,
                                               0,
                                               image.size.width,
                                               image.size.height);
                
                [minimizeBtn addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
                [minimizeBtn setImage:image forState:UIControlStateNormal];
                [currentWebView addSubview:minimizeBtn];
            }
            
        }];
    }
    
    
}


#pragma mark - UIWebViewDelegate


-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    klogdbg_func_arg1s([self getParamsDictionary:request.URL.query]);
    
    if ([[request.URL query] hasPrefix:@"js-player-bridge"]) {
        
        NSDictionary *params = [self getParamsDictionary:request.URL.query];
        NSString *operation = params[@"operation"];
        
        if ([operation isEqualToString:@"minimize"]) {
            
            [self buttonPressed:self.currentButtonShown completion:nil];
        }
        
        else if ([operation isEqualToString:@"close"]) {
            
            [self buttonPressed:self.currentButtonShown completion:^ {
                PlayerActivityButton *activityBtn = [self getButtonForWebView:webView];
                
                if (activityBtn)
                    activityBtn.hidden = YES;
            }];
        }
        // need to open different activity from current show activity
        else if ([operation isEqualToString:@"open"]) {
            
            NSString *activityIdToOpen = params[@"activity_id"];
            PlayerActivityObject *activityObject = [[PlayerActivityManager sharedInstance] activityForActivityId:activityIdToOpen];
            
            // if the wanted activity id to open is exists
            if (activityObject) {
                
                PlayerActivityButton *activityButtonToOpen = [self.activityIdToPlayerActivityButton objectForKey:activityIdToOpen];
                
                if (activityButtonToOpen) {
                 
                    // programmatically press the wanted activity button
                    [self buttonPressed:activityButtonToOpen];
                }
            }
            
            
        }
        return NO;
    }
    
    return YES;
}

-(PlayerActivityButton *)getButtonForWebView: (UIWebView *)webView {
    NSArray *keysForWebView = [self.activityIdToWebView allKeysForObject:webView];
    NSString *key = [keysForWebView lastObject];
    
    PlayerActivityButton *activityBtn = [self.activityIdToPlayerActivityButton objectForKey:key];
    
    return activityBtn;
}


-(NSDictionary*)getParamsDictionary:(NSString*)queryString {
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    for (NSString *param in [queryString componentsSeparatedByString:@"&"]) {
        
        NSArray *elts = [param componentsSeparatedByString:@"="];
        
        if([elts count] < 2) continue;
        
        [params setObject:[elts objectAtIndex:1] forKey:[elts objectAtIndex:0]];
    }
    
    return params;
}

#pragma mark - Docking

-(void)removeView:(UIView *)view {
    
    //Remove the view from previous array
    for (NSString *key in self.activityIconsDictionary) {
        NSMutableArray *array = self.activityIconsDictionary[key];
        BOOL containsObject = [array containsObject:view];
        if (containsObject) {
            [array removeObject:view];
        }
    }
}


-(void)addViewWithDockingHorizontal:(NSString *)horizontal
                           vertical:(NSString *)vertical
                               view:(UIView*)view {
    
    //Remove the view from previous array
    [self removeView:view];
    
    NSString *arrayName = [NSString stringWithFormat:@"%@%@", [vertical substringToIndex:1], [horizontal substringToIndex:1]];
    NSMutableArray *icons = self.activityIconsDictionary[arrayName];
    [icons addObject:view];
    
    view.alpha = 0;
    [self.view addSubview:view];
    
    [UIView animateWithDuration:0.3 animations: ^{
        view.alpha = 1;
        
    }];
    
    
    [self drawIcons:icons vertical:vertical horizintal:horizontal padding:5 isHorizontal:YES];
    
}


//Draw icons for array in the specified direction
-(void)drawIcons: (NSMutableArray *)icons
        vertical:(NSString *)vertical
      horizintal:(NSString*)horizontal
         padding:(NSInteger)padding
    isHorizontal:(BOOL)isHorizontal {
    
    if (isHorizontal) {
        CGPoint origin = [self originForHorizontalArray:icons horizintal:horizontal vertical:vertical padding:5];
        [self drawHorizontalIcons:icons origin:origin vertical:vertical horizintal:horizontal padding:5];
    } else {
        CGPoint origin = [self originForVerticalArray:icons horizintal:horizontal vertical:vertical padding:5];
        [self drawVerticalIcons:icons origin:origin vertical:vertical horizintal:horizontal padding:5];
    }
}


//Calculate the origin of the icons according to the given position and direction - Vertical
-(CGPoint)originForVerticalArray:(NSMutableArray *)icons
                      horizintal:(NSString *)horizontal
                        vertical:(NSString *)vertical
                         padding:(NSInteger) padding {
    
    NSInteger y = 0;
    NSInteger x = 0;
    
    if ([horizontal caseInsensitiveCompare:@"left"] == NSOrderedSame) {
        x = 0;
        
    } else if ([horizontal caseInsensitiveCompare:@"center"] == NSOrderedSame) {
        x = self.view.bounds.size.width / 2;
        
    } else if ([horizontal caseInsensitiveCompare:@"right"] == NSOrderedSame) {
        x = self.view.bounds.size.width;
    }
    
    if ([vertical caseInsensitiveCompare:@"top"] == NSOrderedSame) {
        y = 0;
        
    } else if ([vertical caseInsensitiveCompare:@"middle"] == NSOrderedSame) {
        
        for (UIView *view in icons) {
            y += view.frame.size.height + padding;
        }
        
        NSInteger center = self.view.bounds.size.height / 2;
        y = center - (y / 2);
        
    } else if ([vertical caseInsensitiveCompare:@"bottom"] == NSOrderedSame) {
        y = self.view.bounds.size.height;
    }
    
    CGPoint origin = CGPointMake(x, y);
    return origin;
}


//Draw the icons vertically
-(void)drawVerticalIcons: (NSMutableArray *)icons
                  origin:(CGPoint)origin
                vertical:(NSString *)vertical
              horizintal:(NSString*)horizontal
                 padding:(NSInteger)padding {
    
    NSInteger x = origin.x;
    NSInteger y = origin.y;
    
    for (UIView *btn in icons) {
        
        NSInteger newX = 0;
        
        if ([horizontal caseInsensitiveCompare:@"right"] == NSOrderedSame) {
            newX = x - btn.frame.size.width;
            
        } else if ([horizontal caseInsensitiveCompare:@"center"] == NSOrderedSame) {
            newX = x - (btn.frame.size.width / 2);
            
        } else if ([horizontal caseInsensitiveCompare:@"left"] == NSOrderedSame) {
            newX = 0;
        }
        
        if ([vertical caseInsensitiveCompare:@"bottom"] == NSOrderedSame) {
            y -= btn.frame.size.height;
            btn.frame = CGRectMake(newX, y, btn.frame.size.width, btn.frame.size.height);
            y -= padding;
            
            continue;
        }
        
        btn.frame = CGRectMake(newX, y, btn.frame.size.width, btn.frame.size.height);
        y += btn.frame.size.width + padding;
    }
    
}


//Calculate the origin of the icons according to the given position and direction - Horizontal
-(CGPoint)originForHorizontalArray:(NSMutableArray *)icons
                        horizintal:(NSString *)horizontal
                          vertical:(NSString *)vertical
                           padding:(NSInteger) padding {
    
    NSInteger y = 0;
    NSInteger x = 0;
    
    if ([horizontal caseInsensitiveCompare:@"left"] == NSOrderedSame) {
        y = 0;
        
    } else if ([horizontal caseInsensitiveCompare:@"center"] == NSOrderedSame) {
        for (UIView *view in icons) {
            x += view.frame.size.width + padding;
        }
        
        NSInteger center = self.view.bounds.size.width / 2;
        x = center - (x / 2);
        
    } else if ([horizontal caseInsensitiveCompare:@"right"] == NSOrderedSame) {
        for (UIView *view in icons) {
            x += view.frame.size.width + padding;
        }
        
        x = self.view.bounds.size.width - x + padding;
    }
    
    if ([vertical caseInsensitiveCompare:@"top"] == NSOrderedSame) {
        y = 0;
        
    } else if ([vertical caseInsensitiveCompare:@"middle"] == NSOrderedSame) {
        y = (self.view.bounds.size.height / 2);
        
    } else if ([vertical caseInsensitiveCompare:@"bottom"] == NSOrderedSame) {
        y = self.view.bounds.size.height;
    }
    
    CGPoint origin = CGPointMake(x, y);
    return origin;
}


//Draw the icons horizontally
-(void)drawHorizontalIcons: (NSMutableArray *)icons
                    origin:(CGPoint)origin
                  vertical:(NSString *)vertical
                horizintal:(NSString*)horizontal
                   padding:(NSInteger) padding {
    
    NSInteger x = origin.x;
    NSInteger y = origin.y;
    
    for (UIView *btn in icons) {
        
        if ([vertical caseInsensitiveCompare:@"middle"] == NSOrderedSame) {
            NSInteger middleY = y - (btn.frame.size.height / 2);
            btn.frame = CGRectMake(x, middleY, btn.frame.size.width, btn.frame.size.height);
            x += btn.frame.size.width + padding;
            continue;
            
        } else if ([vertical caseInsensitiveCompare:@"bottom"] == NSOrderedSame) {
            NSInteger middleY = y - btn.frame.size.height;
            btn.frame = CGRectMake(x, middleY, btn.frame.size.width, btn.frame.size.height);
            x += btn.frame.size.width + padding;
            continue;
        }
        
        btn.frame = CGRectMake(x, y, btn.frame.size.width, btn.frame.size.height);
        x += btn.frame.size.width + padding;
    }
}


@end
