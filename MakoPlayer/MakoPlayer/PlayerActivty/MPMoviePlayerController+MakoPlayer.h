//
//  MPMoviePlayerController+MakoPlayer.h
//  MakoPlayer
//
//  Created by Ran Greenberg on 5/1/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>
#import "VideoItem.h"

typedef enum {
    PLAYER_ACTIVITY_ANIMATION_NONE,
    PLAYER_ACTIVITY_ANIMATION_FADE,
    PLAYER_ACTIVITY_ANIMATION_SLIDE,
    PLAYER_ACTIVITY_ANIMATION_GENIE
}PLAYER_ACTIVITY_ANIMATION;

@interface MPMoviePlayerController (MakoPlayer)


/*! This method add UIWebView on top of the MPMoviePlayer view !*/
-(void)startActivitiesOnPlayerWithVideoItem:(id<VideoItem>)videoItem withActivitiesPath:(NSString*)path;
-(void)stopActivitiesOnPlayer;


@end
