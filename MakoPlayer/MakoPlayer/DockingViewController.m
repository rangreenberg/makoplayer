//
//  DockingViewController.m
//  MakoPlayer
//
//  Created by Rona Assouline on 5/5/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import "DockingViewController.h"

@interface DockingViewController ()

//@property (strong, nonatomic) NSMutableArray *topLeft;
//@property (strong, nonatomic) NSMutableArray *topCenter;
//@property (strong, nonatomic) NSMutableArray *topRight;
//
//@property (strong, nonatomic) NSMutableArray *middleLeft;
//@property (strong, nonatomic) NSMutableArray *middleCenter;
//@property (strong, nonatomic) NSMutableArray *middleRight;
//
//@property (strong, nonatomic) NSMutableArray *bottomLeft;
//@property (strong, nonatomic) NSMutableArray *bottomCenter;
//@property (strong, nonatomic) NSMutableArray *bottomRight;

@property (strong, nonatomic) UIView *testView;

@property (strong, nonatomic) NSDictionary *activityIconsDictionary;
@end

@implementation DockingViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.activityIconsDictionary = @{
                           @"tl":[[NSMutableArray alloc] init],
                           @"tc":[[NSMutableArray alloc] init],
                           @"tr":[[NSMutableArray alloc] init],
                           @"ml":[[NSMutableArray alloc] init],
                           @"mc":[[NSMutableArray alloc] init],
                           @"mr":[[NSMutableArray alloc] init],
                           @"bl":[[NSMutableArray alloc] init],
                           @"bc":[[NSMutableArray alloc] init],
                           @"br":[[NSMutableArray alloc] init]
                           };
}

-(BOOL)shouldAutorotate {
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscapeLeft;
}

- (IBAction)topLeft:(id)sender {
    int lowerBound = 10;
    int upperBound = 50;
    int rndValue = lowerBound + arc4random() % (upperBound - lowerBound);
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rndValue, rndValue)];
    [self addViewWithDockingHorizontal:@"left" vertical:@"top" view:view];

    //[self setDockingHorizontal:@"left" vertical:@"top"];
}

- (IBAction)middleLeft:(id)sender {
    int lowerBound = 10;
    int upperBound = 50;
    int rndValue = lowerBound + arc4random() % (upperBound - lowerBound);
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rndValue, rndValue)];
    [self addViewWithDockingHorizontal:@"left" vertical:@"middle" view:view];

    //[self setDockingHorizontal:@"left" vertical:@"middle"];
}

- (IBAction)bottomLeft:(id)sender {
    int lowerBound = 10;
    int upperBound = 50;
    int rndValue = lowerBound + arc4random() % (upperBound - lowerBound);
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rndValue, rndValue)];
    [self addViewWithDockingHorizontal:@"left" vertical:@"bottom" view:view];

    //[self setDockingHorizontal:@"left" vertical:@"bottom"];
}

- (IBAction)bottomCenter:(id)sender {
    int lowerBound = 10;
    int upperBound = 50;
    int rndValue = lowerBound + arc4random() % (upperBound - lowerBound);
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rndValue, rndValue)];
    [self addViewWithDockingHorizontal:@"center" vertical:@"bottom" view:view];

    //[self setDockingHorizontal:@"center" vertical:@"bottom"];
}

- (IBAction)bottomRight:(id)sender {
    int lowerBound = 10;
    int upperBound = 50;
    int rndValue = lowerBound + arc4random() % (upperBound - lowerBound);
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rndValue, rndValue)];
    [self addViewWithDockingHorizontal:@"right" vertical:@"bottom" view:view];

    //[self setDockingHorizontal:@"right" vertical:@"bottom"];
}

- (IBAction)middleCenter:(id)sender {
    int lowerBound = 10;
    int upperBound = 50;
    int rndValue = lowerBound + arc4random() % (upperBound - lowerBound);
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rndValue, rndValue)];
    [self addViewWithDockingHorizontal:@"center" vertical:@"middle" view:view];

    //[self setDockingHorizontal:@"center" vertical:@"middle"];
}

- (IBAction)middleRight:(id)sender {
    int lowerBound = 10;
    int upperBound = 50;
    int rndValue = lowerBound + arc4random() % (upperBound - lowerBound);
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rndValue, rndValue)];
    [self addViewWithDockingHorizontal:@"right" vertical:@"middle" view:view];

//[self setDockingHorizontal:@"right" vertical:@"middle"];
}

- (IBAction)topCenter:(id)sender {
    int lowerBound = 10;
    int upperBound = 50;
    int rndValue = lowerBound + arc4random() % (upperBound - lowerBound);
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rndValue, rndValue)];
    [self addViewWithDockingHorizontal:@"center" vertical:@"top" view:view];
    view.backgroundColor = [UIColor redColor];
    [self.view addSubview:view];
    //[self setDockingHorizontal:@"center" vertical:@"top"];
}

- (IBAction)topRight:(id)sender {
    int lowerBound = 10;
    int upperBound = 50;
    int rndValue = lowerBound + arc4random() % (upperBound - lowerBound);
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rndValue, rndValue)];
    [self addViewWithDockingHorizontal:@"right" vertical:@"top" view:view];

    //[self setDockingHorizontal:@"right" vertical:@"top"];
}

-(void)removeView:(UIView *)view {
    //Remove the view from previous array
    for (NSString *key in self.activityIconsDictionary) {
        NSMutableArray *array = self.activityIconsDictionary[key];
        BOOL containsObject = [array containsObject:view];
        if (containsObject) {
            [array removeObject:view];
        }
    }
}

-(void)addViewWithDockingHorizontal:(NSString *)horizontal
                           vertical:(NSString *)vertical
                               view:(UIView*)view {
    
    //Remove the view from previous array
    [self removeView:view];
    
    NSString *arrayName = [NSString stringWithFormat:@"%@%@", [vertical substringToIndex:1], [horizontal substringToIndex:1]];
    NSMutableArray *icons = self.activityIconsDictionary[arrayName];
    [icons addObject:view];
    
    //for debugging
    view.backgroundColor = [UIColor redColor];
    [self.view addSubview:view];
    
    [self drawIcons:icons vertical:vertical horizintal:horizontal padding:5 isHorizontal:YES];
    
}

//Draw icons for array in the specified direction
-(void)drawIcons: (NSMutableArray *)icons
                vertical:(NSString *)vertical
              horizintal:(NSString*)horizontal
                 padding:(NSInteger)padding
            isHorizontal:(BOOL)isHorizontal {
    
    if (isHorizontal) {
        CGPoint origin = [self originForHorizontalArray:icons horizintal:horizontal vertical:vertical padding:5];
        [self drawHorizontalIcons:icons origin:origin vertical:vertical horizintal:horizontal padding:5];
    } else {
        CGPoint origin = [self originForVerticalArray:icons horizintal:horizontal vertical:vertical padding:5];
        [self drawVerticalIcons:icons origin:origin vertical:vertical horizintal:horizontal padding:5];
    }
}

//Calculate the origin of the icons according to the given position and direction - Vertical
-(CGPoint)originForVerticalArray:(NSMutableArray *)icons
                      horizintal:(NSString *)horizontal
                        vertical:(NSString *)vertical
                         padding:(NSInteger) padding {
    
    NSInteger y = 0;
    NSInteger x = 0;
    
    if ([horizontal caseInsensitiveCompare:@"left"] == NSOrderedSame) {
        x = 0;
        
    } else if ([horizontal caseInsensitiveCompare:@"center"] == NSOrderedSame) {
        x = self.view.bounds.size.width / 2;
        
    } else if ([horizontal caseInsensitiveCompare:@"right"] == NSOrderedSame) {
        x = self.view.bounds.size.width;
    }
    
    if ([vertical caseInsensitiveCompare:@"top"] == NSOrderedSame) {
        y = 0;
        
    } else if ([vertical caseInsensitiveCompare:@"middle"] == NSOrderedSame) {
        
        for (UIView *view in icons) {
            y += view.frame.size.height + padding;
        }
        
        NSInteger center = self.view.bounds.size.height / 2;
        y = center - (y / 2);
        
    } else if ([vertical caseInsensitiveCompare:@"bottom"] == NSOrderedSame) {
        y = self.view.bounds.size.height;
    }
    
    CGPoint origin = CGPointMake(x, y);
    return origin;
}


//Draw the icons vertically
-(void)drawVerticalIcons: (NSMutableArray *)icons
                  origin:(CGPoint)origin
                vertical:(NSString *)vertical
              horizintal:(NSString*)horizontal
                 padding:(NSInteger)padding {
    
    NSInteger x = origin.x;
    NSInteger y = origin.y;
    
    for (UIView *btn in icons) {

        NSInteger newX = 0;
        
        if ([horizontal caseInsensitiveCompare:@"right"] == NSOrderedSame) {
            newX = x - btn.frame.size.width;

        } else if ([horizontal caseInsensitiveCompare:@"center"] == NSOrderedSame) {
            newX = x - (btn.frame.size.width / 2);
            
        } else if ([horizontal caseInsensitiveCompare:@"left"] == NSOrderedSame) {
            newX = 0;
        }

        if ([vertical caseInsensitiveCompare:@"bottom"] == NSOrderedSame) {
            y -= btn.frame.size.height;
            btn.frame = CGRectMake(newX, y, btn.frame.size.width, btn.frame.size.height);
            y -= padding;

            continue;
        }
        
        btn.frame = CGRectMake(newX, y, btn.frame.size.width, btn.frame.size.height);
        y += btn.frame.size.width + padding;
    }
    
}

//Calculate the origin of the icons according to the given position and direction - Horizontal
-(CGPoint)originForHorizontalArray:(NSMutableArray *)icons
                        horizintal:(NSString *)horizontal
                          vertical:(NSString *)vertical
                           padding:(NSInteger) padding {
    
    NSInteger y = 0;
    NSInteger x = 0;
    
    if ([horizontal caseInsensitiveCompare:@"left"] == NSOrderedSame) {
        y = 0;
        
    } else if ([horizontal caseInsensitiveCompare:@"center"] == NSOrderedSame) {
        for (UIView *view in icons) {
            x += view.frame.size.width + padding;
        }
        
        NSInteger center = self.view.bounds.size.width / 2;
        x = center - (x / 2);
        
    } else if ([horizontal caseInsensitiveCompare:@"right"] == NSOrderedSame) {
        for (UIView *view in icons) {
            x += view.frame.size.width + padding;
        }
        
        x = self.view.bounds.size.width - x + padding;
    }
    
    if ([vertical caseInsensitiveCompare:@"top"] == NSOrderedSame) {
        y = 0;
        
    } else if ([vertical caseInsensitiveCompare:@"middle"] == NSOrderedSame) {
        y = (self.view.bounds.size.height / 2);
        
    } else if ([vertical caseInsensitiveCompare:@"bottom"] == NSOrderedSame) {
        y = self.view.bounds.size.height;
    }
    
    CGPoint origin = CGPointMake(x, y);
    return origin;
}


//Draw the icons horizontally
-(void)drawHorizontalIcons: (NSMutableArray *)icons
                    origin:(CGPoint)origin
                  vertical:(NSString *)vertical
                horizintal:(NSString*)horizontal
                   padding:(NSInteger) padding {
    
    NSInteger x = origin.x;
    NSInteger y = origin.y;
    
    for (UIView *btn in icons) {
        
        if ([vertical caseInsensitiveCompare:@"middle"] == NSOrderedSame) {
            NSInteger middleY = y - (btn.frame.size.height / 2);
            btn.frame = CGRectMake(x, middleY, btn.frame.size.width, btn.frame.size.height);
            x += btn.frame.size.width + padding;
            continue;
            
        } else if ([vertical caseInsensitiveCompare:@"bottom"] == NSOrderedSame) {
            NSInteger middleY = y - btn.frame.size.height;
            btn.frame = CGRectMake(x, middleY, btn.frame.size.width, btn.frame.size.height);
            x += btn.frame.size.width + padding;
            continue;
        }
        
        btn.frame = CGRectMake(x, y, btn.frame.size.width, btn.frame.size.height);
        x += btn.frame.size.width + padding;
    }
}

@end
