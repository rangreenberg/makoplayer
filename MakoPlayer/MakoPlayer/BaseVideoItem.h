//
//  BaseVideoItem.h
//  MakoTV2
//
//  Created by Sagi Mann on 9/12/13.
//  Copyright (c) 2013 Tomer Har Yoffi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VideoItem.h"



@interface BaseVideoItem : NSObject
<VideoItem>



// init the video item with an underlying data structure
// used by most derived classes.
- (id)initWithData:(id)data;



/** utility methods used by derived classes to extract items from the data structure.
 It takes care of NSNull values (converted to nil),
 nil strings are converted to empty strings.
 paths are converted to full URLs in case they are partial
 */
- (id)objectForKey:(NSString*)key;
- (id)objectForKey:(NSString*)key inData:(id)data;
- (NSString*)stringForKey:(NSString*)key;
- (NSString*)stringForKey:(NSString*)key inData:(id)data;
- (NSURL*)urlForKey:(NSString*)key;
- (NSURL*)urlForKey:(NSString*)key inData:(id)data;
- (BOOL)boolForKey:(NSString*)key;
- (BOOL)boolForKey:(NSString*)key inData:(id)data;
- (int)intForKey:(NSString*)key;
- (int)intForKey:(NSString*)key inData:(id)data;
- (double)doubleForKey:(NSString*)key;
- (double)doubleForKey:(NSString*)key inData:(id)data;



@end
